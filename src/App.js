import React, { useState, useEffect } from 'react';
import './App.css';
import { ImagesGrid } from './components/images-grid/ImagesGrid';
import { Loader } from './components/loader/Loader';
import styled from 'styled-components';
import InfinteScroll from 'react-infinite-scroll-component';
import { SRLWrapper } from "simple-react-lightbox";

import axios from 'axios';

const WrapImg = styled.section`
  display:grid;
  grid-gap:1em;
  grid-template-columns:repeat(auto-fit,minmax(250px, 1fr));
  grid-auto-rows:300px;
  max-width:100vw
`;
function App() {
  const [images, setImages] = useState([]);

  useEffect(() => {
    fetchImages();
  }, []);

  const fetchImages = () => {
    const apiDomain = "https://api.unsplash.com";
    const accessKey = "hiZFGh2gKECZ1NQEPVXzeRzD6Po5nBPzBz56JlCodlY";
    // `${apiDomain}/photos?client_id=${accessKey}&page=1`
    axios.get(`${apiDomain}/photos/random?client_id=${accessKey}&count=10`).then(
      response => setImages([...images, ...response.data]));
  }
  return (
    <div className="App">

      <h1>Quiz NBS Result</h1>
      <InfinteScroll
        dataLength={images.length}
        next={fetchImages}
        hasMore={true}
        loader={<Loader />}>

        <SRLWrapper>
          <WrapImg>
            {images.map(image => (
              <div key={image.id}>
                <a href={image.urls.regular}>
                  <ImagesGrid url={image.urls.thumb} />
                </a>
              </div>
            ))}
          </WrapImg>
        </SRLWrapper>

      </InfinteScroll>


    </div>
  );
}

export default App;
