import React from 'react';
import './Loader.css';

export const Loader = () => {
    return (
        <div className="loading">
            <div class="lds-facebook"><div></div><div></div><div></div></div>
        </div>
    )
}