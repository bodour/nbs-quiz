import React from 'react';
import styled from 'styled-components';

const Image = styled.img`
    width:100%;
    height:100%;
    object-fit:cover
`;

export const ImagesGrid = ({ key, url }) => {
    return <Image key={key} src={url} />

}
